package com.satan.fullmetaltac.geocontentprovider

import android.app.Application
import com.satan.fullmetaltac.geocontentprovider.data.RoutesDatabase

class AppDB : Application() {
    override fun onCreate() {
        super.onCreate()
        RoutesDatabase.DB.initDB(this)
    }
}