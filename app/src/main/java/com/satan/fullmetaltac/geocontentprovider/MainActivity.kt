package com.satan.fullmetaltac.geocontentprovider

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.satan.fullmetaltac.geocontentprovider.data.Routes
import kotlinx.coroutines.experimental.async


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        async {
            var outputText = "";
            val cursor = contentResolver.query(GeoDataProvider.URI, arrayOf(Routes.COLUMN_ID, Routes.TIME), null, null)

            if (cursor.moveToFirst()) {
                outputText = cursor.columnNames.joinToString() + "\n"
                outputText += "${cursor.getLong(0)}/" +
                        "${cursor.getLong(1)}/" +
                        "${cursor.getLong(2)}/" +
                        "${cursor.getLong(3)}/" +
                        "${cursor.getLong(4)}/" +
                        "${cursor.getLong(5)}" +
                        "\n"
                while (cursor.moveToNext()) {
                    outputText += "${cursor.getLong(0)}/" +
                            "${cursor.getLong(1)}/" +
                            "${cursor.getLong(2)}/" +
                            "${cursor.getLong(3)}/" +
                            "${cursor.getLong(4)}/" +
                            "${cursor.getLong(5)}" +
                            "\n"
                }
            }
            findViewById<TextView>(R.id.text_output_main_activity).text = outputText
        }
    }
}
