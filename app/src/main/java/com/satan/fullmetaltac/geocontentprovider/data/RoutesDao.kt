package com.satan.fullmetaltac.geocontentprovider.data

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import android.database.Cursor

@Dao
interface RoutesDao {

    @Query("SELECT COUNT(*) FROM " + Routes.TABLE_NAME)
    fun count(): Int

    @Query("SELECT * FROM " + Routes.TABLE_NAME)
    fun selectAll(): Cursor

    @Insert
    fun insert(routes: Routes): Long

    @Insert
    fun insert(routes: List<Routes>)

    @Query("DELETE FROM " + Routes.TABLE_NAME + " WHERE " + Routes.COLUMN_ID + " = :id")
    fun deleteById(id: Long): Int

    @Query("SELECT * FROM " + Routes.TABLE_NAME + " WHERE " + Routes.COLUMN_ID + " = :id")
    fun selectById(id: Long): Cursor

    @Update
    fun update(routes: Routes): Int

}