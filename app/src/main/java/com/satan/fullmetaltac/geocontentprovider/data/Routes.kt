package com.satan.fullmetaltac.geocontentprovider.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.content.ContentValues
import android.provider.BaseColumns

@Entity(tableName = Routes.TABLE_NAME)
class Routes(
        @ColumnInfo(name = Routes.TIME)
        var time: Long,

        @ColumnInfo(name = Routes.FROM_LATITUDE)
        var fromLatitude: Long,

        @ColumnInfo(name = Routes.FROM_LONGITUDE)
        var fromLongitude: Long,

        @ColumnInfo(name = Routes.TO_LATITUDE)
        var toLatitude: Long,

        @ColumnInfo(name = Routes.TO_LONGITUDE)
        var toLongitude: Long,

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(index = true, name = Routes.COLUMN_ID)
        var id: Long = 0
) {
    companion object {
        const val TABLE_NAME = "routes"
        const val COLUMN_ID = BaseColumns._ID
        const val TIME = "time"
        const val FROM_LATITUDE = "from_lat"
        const val FROM_LONGITUDE = "from_lon"
        const val TO_LATITUDE = "to_lat"
        const val TO_LONGITUDE = "to_lon"

        fun fromContentValues(vals: ContentValues): Routes {
            var route = Routes(0, 0, 0, 0, 0)

            if (vals.containsKey(COLUMN_ID)) {
                route.id = vals.getAsLong(COLUMN_ID)
            }
            if (vals.containsKey(TIME)) {
                route.time = vals.getAsLong(TIME)
            }
            if (vals.containsKey(FROM_LATITUDE)) {
                route.fromLatitude = vals.getAsLong(FROM_LATITUDE)
            }
            if (vals.containsKey(FROM_LONGITUDE)) {
                route.fromLongitude = vals.getAsLong(FROM_LONGITUDE)
            }
            if (vals.containsKey(TO_LATITUDE)) {
                route.toLatitude = vals.getAsLong(TO_LATITUDE)
            }
            if (vals.containsKey(TO_LONGITUDE)) {
                route.toLongitude = vals.getAsLong(TO_LONGITUDE)
            }

            return route
        }
    }
}