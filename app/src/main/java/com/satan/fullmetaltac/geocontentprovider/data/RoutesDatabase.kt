package com.satan.fullmetaltac.geocontentprovider.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.satan.fullmetaltac.geocontentprovider.BuildConfig
import kotlinx.coroutines.experimental.async
import java.util.*

@Database(entities = arrayOf(Routes::class), version = BuildConfig.DB_VERSION)
abstract class RoutesDatabase : RoomDatabase() {

    abstract fun routesDao(): RoutesDao

    object DB {
        var Instance: RoutesDatabase? = null

        fun initDB(context: Context) =
                Room.databaseBuilder(context, RoutesDatabase::class.java, BuildConfig.DB_NAME)
                        .build()
                        .also { Instance = it }
                        .also { insertDummyData() }

        private fun insertDummyData() {
            async {
                if (Instance?.routesDao()?.count() == 0) {
                    Instance?.routesDao()?.insert((0..5).map { Routes(rand(), rand(), rand(), rand(), rand()) })
                }
            }
        }

        private fun rand(from: Int = 0, to: Int = 200): Long {
            return (Random().nextInt(to - from) + from).toLong()
        }
    }
}