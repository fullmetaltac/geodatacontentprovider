package com.satan.fullmetaltac.geocontentprovider

import android.content.*
import android.database.Cursor
import android.net.Uri
import com.satan.fullmetaltac.geocontentprovider.data.Routes
import com.satan.fullmetaltac.geocontentprovider.data.RoutesDatabase
import java.io.File

class GeoDataProvider : ContentProvider() {

    val matcher: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)

    init {
        matcher.addURI(AUTHORITY, Routes.TABLE_NAME, CODE_ROUTES_ALL)
        matcher.addURI(AUTHORITY, Routes.TABLE_NAME + "/*", CODE_ROUTES_ITEM)
    }

    companion object {
        private const val AUTHORITY: String = "com.satan.fullmetaltac.geocontentprovider"
        val URI = Uri.parse("content://" + AUTHORITY + File.separator + Routes.TABLE_NAME)
        val CODE_ROUTES_ALL = 1
        val CODE_ROUTES_ITEM = 2

    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return when (matcher.match(uri)) {
            CODE_ROUTES_ALL -> {
                val database = RoutesDatabase.DB.Instance
                val id = database?.routesDao()?.insert(Routes.fromContentValues(values as ContentValues))
                context.contentResolver.notifyChange(uri, null)
                ContentUris.withAppendedId(uri, id!!)
            }
            CODE_ROUTES_ITEM -> {
                throw IllegalArgumentException("Invalid URI, cannot insert with ID: " + uri)
            }
            else -> {
                throw IllegalArgumentException("Unknown URI: " + uri);
            }
        }
    }

    override fun query(uri: Uri, projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?): Cursor? {
        val code = matcher.match(uri)
        return if (code == CODE_ROUTES_ALL || code == CODE_ROUTES_ITEM) {
            val dao = RoutesDatabase.DB.Instance?.routesDao()
            if (code == CODE_ROUTES_ALL) {
                val cursor = dao?.selectAll()
                cursor?.setNotificationUri(context.contentResolver, uri)
                cursor
            } else {
                val cursor = dao?.selectById(ContentUris.parseId(uri))
                cursor?.setNotificationUri(context.contentResolver, uri)
                cursor
            }
        } else {
            throw IllegalArgumentException("Unknown URI: " + uri)
        }
    }

    override fun onCreate(): Boolean {
        return true
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        return when (matcher.match(uri)) {
            CODE_ROUTES_ALL -> {
                throw IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            }
            CODE_ROUTES_ITEM -> {
                val routes = Routes.fromContentValues(values as ContentValues)
                routes.id = ContentUris.parseId(uri)
                val count = RoutesDatabase.DB.Instance?.routesDao()?.update(routes)
                context.contentResolver.notifyChange(uri, null)
                count!!
            }
            else -> {
                throw IllegalArgumentException("Unknown URI: " + uri)
            }
        }
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        return when (matcher.match(uri)) {
            CODE_ROUTES_ALL -> {
                throw IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            }
            CODE_ROUTES_ITEM -> {
                val count = RoutesDatabase.DB.Instance?.routesDao()
                        ?.deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                count!!
            }
            else -> {
                throw IllegalArgumentException("\"Unknown URI: \" + uri")
            }
        }
    }

    override fun getType(uri: Uri): String? {
        return when (matcher.match(uri)) {
            CODE_ROUTES_ALL -> {
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + AUTHORITY + Routes.TABLE_NAME
            }
            CODE_ROUTES_ITEM -> {
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + AUTHORITY + Routes.TABLE_NAME
            }
            else -> {
                throw IllegalAccessException("Unknown URI: " + uri)
            }
        }
    }
}